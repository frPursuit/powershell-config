# Utility functions

function concat() {
    [CmdletBinding(PositionalBinding=$true)]
    param(
        [string[]] $first,
        [string[]] $second
    )

    if($null -eq $first) {
        return $second
    }
    if($null -eq $second) {
        return $first
    }

    # Create a single string[] with all elements
    if(-not ($first -is [array])) {
        $first = @($first)
    }
    if(-not ($second -is [array])) {
        $second = @($second)
    }

    return $first + $second
}

function delegate {
    [CmdletBinding(PositionalBinding=$false)]
    param(
        [Parameter(ValueFromRemainingArguments)]
        [string[]] $command
    )
    
    return {
        [CmdletBinding(PositionalBinding=$false)]
        param(
            [Parameter(ValueFromPipeline)]
            $pipeData,
            
            [Parameter(ValueFromRemainingArguments)]
            [string[]] $clArgs
        )
    
        begin
        {
            $aliasCommand = $command[0]
            $aliasArgs = if($command.count -gt 1) { $command[1..($command.count-1)] } else { @() }

            # Set up a steppable pipeline.
            $scriptCmd = { &$aliasCommand $(concat $aliasArgs $clArgs) }
            $steppablePipeline = $scriptCmd.GetSteppablePipeline($myInvocation.CommandOrigin)
            $steppablePipeline.Begin($PSCmdlet)
        }
    
        process
        {
            # Pass the current pipeline input through.
            $steppablePipeline.Process($pipeData)
        }
    
        end
        {
            $steppablePipeline.End()
        }
    }.GetNewClosure()
}

function wsl_delegate {
    [CmdletBinding(PositionalBinding=$false)]
    param(
        [Parameter(ValueFromRemainingArguments)]
        [string[]] $command
    )

    return delegate $(concat wsl $command)
}

function setenv {
    param ([string] $name, [string] $value)
    Set-Item -Path "Env:$name" -Value $value
}


# Remove aliases

foreach($alias in @("ls", "rm", "rmdir", "kill")) {
    Remove-Alias -Name $alias
}


# Add aliases

$function:which = delegate "$env:windir\System32\where.exe"
$function:ls = delegate "$env:GIT_INSTALL_ROOT/usr/bin/ls" --color=auto
$function:ll = delegate ls -lAFh
$function:la = delegate ls -a
$function:lla = delegate ll -a
$function:grep = delegate "$env:GIT_INSTALL_ROOT/usr/bin/grep" --color=auto


# WSL passthrough commands

$function:ansible = wsl_delegate ansible
Set-Item -Path "function:ansible-playbook" -Value $(wsl_delegate ansible-playbook)
Set-Item -Path "function:ansible-vault" -Value $(wsl_delegate ansible-vault)
Set-Item -Path "function:ansible-galaxy" -Value $(wsl_delegate ansible-galaxy)
Set-Item -Path "function:ansible-console" -Value $(wsl_delegate ansible-console)
Set-Item -Path "function:ansible-config" -Value $(wsl_delegate ansible-config)
Set-Item -Path "function:ansible-doc" -Value $(wsl_delegate ansible-doc)
Set-Item -Path "function:ansible-inventory" -Value $(wsl_delegate ansible-inventory)
Set-Item -Path "function:ansible-pull" -Value $(wsl_delegate ansible-pull)


# PATH
$env:PATH = $env:PATH + ";$env:GIT_INSTALL_ROOT\usr\bin"


# ls colors
Invoke-Expression $(dircolors -c $env:USERPROFILE/Documents/PowerShell/dir_colors)

# Disable predictive completion
Set-PSReadLineOption -PredictionSource None

# Prompt

function prompt {
    $ESC = [char]27
    $User = [Security.Principal.WindowsIdentity]::GetCurrent();
    $Date = Get-Date -Format 'H:mm'
    $IsAdmin = (New-Object Security.Principal.WindowsPrincipal $User).IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
    $Username = $User.Name.split("\")[1]
    $HostName = [System.Net.Dns]::GetHostName()

    # Window title
    $host.ui.RawUI.WindowTitle = "$($Username)@$($HostName): $($pwd)"

    # Prompt
    $HostColor = "$ESC[0;38;5;75m"

    if($IsAdmin) {
        return "$ESC[1;36m$($Date) $ESC[1;31m$Username$ESC[1;33m@$HostColor$HostName $ESC[1;32m$pwd $ESC[1;33m#$ESC[0m "
    }
    else {
        return "$ESC[1;36m$($Date) $ESC[0;1m$Username$ESC[1;33m@$HostColor$HostName $ESC[1;32m$pwd $ESC[1;33m%$ESC[0m "
    }
}


# Welcome message

$winVersion = [System.Environment]::OSVersion.Version
$startupDate = Get-Date -Format "dddd MM/dd/yyyy H:mm"
$startupDate = [System.Char]::ToUpper($startupDate[0]) + $startupDate.Substring(1)

Write-Host "Microsoft Windows $winVersion"
Write-Host $startupDate
Write-Host ""
