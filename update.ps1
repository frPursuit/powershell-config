##############################################################
# Utility script used to update the PowerShell configuration #
#                 from the GitLab repository                 #
##############################################################

echo "Updating PowerShell configuration..."

$BASE_URL="https://gitlab.com/frPursuit/powershell-config/-/raw/master"
$USER_DIR="$env:USERPROFILE/Documents"

if(!(Test-Path -Path $USER_DIR/PowerShell))
{
    mkdir $USER_DIR/PowerShell
}

echo "Updating directory colors"
curl -s "$BASE_URL/PowerShell/dir_colors" > $USER_DIR/PowerShell/dir_colors

echo "Updating Profile.ps1"
curl -s "$BASE_URL/PowerShell/Profile.ps1" > $USER_DIR/PowerShell/Profile.ps1
