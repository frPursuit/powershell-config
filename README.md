# PowerShell Config

Configuration for the [PowerShell](https://en.wikipedia.org/wiki/PowerShell) shell.

The configuration can be automatically updated with the [update.ps1](./update.ps1) script, using the following Command:

    curl -s https://gitlab.com/frPursuit/powershell-config/-/raw/master/update.ps1 | pwsh
